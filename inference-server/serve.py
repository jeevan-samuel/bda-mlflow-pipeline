import os
import sys
print("Running serving script")
print(sys.argv)
model_artefact_loc = sys.argv[1]
port_val = sys.argv[2]
print('mlflow models serve -m {MDL_LOC} -p {PORT} -h 0.0.0.0'.format(MDL_LOC=model_artefact_loc, PORT=port_val))
os.system('mlflow models serve -m {MDL_LOC} -p {PORT} -h 0.0.0.0 --no-conda'.format(MDL_LOC=model_artefact_loc, PORT=port_val))
print('mlflow models serve is now running')
